#include <iostream>
#include <string>
#include "../Generated/SignOperation.grpc.pb.h"
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "Validation.h"
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
	std::string date;
	grpc_init();
	ClientContext context;
	auto hello_stub = SignService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	SignRequest request;
	do {
		std::cout << "\nIntroduceti data: ";
		std::cin >> date;
		if (!valid(date))
		{
			std::cout << "\nData nu este valida!";
		}
	} while (!valid(date));
	request.set_date(date);
	SignReply response;
	auto status = hello_stub->GetSign(&context, request, &response);
	std::cout << "\nZodia dumneavoastra este " << response.sign() << '\n';
}
