#pragma once
#include <string>

bool isLeap(int year)
{
    if (year % 4 == 0)
    {
        if (year % 100 == 0)
        {
            if (year % 400 == 0)
                return true;
            return false;
        }
        return true;
    }
    return false;
}

bool valid(std::string date)
{
    if (date.length() < 7)
        return false;
    bool ok = false;
    for (int i = 0; i < date.length(); i++)
    {
        if (i == 2 || i == 5)
        {
            if (date[i] != '/')
                return false;
        }
        else
        {
            if (date[i] < '0' || date[i] > '9')
                return false;
        }
    }
    if (date[2] != '/' || date[5] != '/')
        return false;

    int month = (date[0] - 48) * 10 + (date[1] - 48);
    int day = (date[3] - 48) * 10 + (date[4] - 48);
    int year = 0;

    for (int i = 6; i < date.length(); i++)
    {
        year = year * 10 + (date[i] - 48);
    }

    if (month < 1 || month > 12)
        return false;

    if (day < 1 || day > 31)
        return false;

    if (month == 2)
    {
        if (isLeap(year))
            return (day <= 29);
        else
            return (day <= 28);
    }

    if (month == 4 || month == 6 || month == 9 || month == 11)
        return (day <= 30);

}