#include "AdditionServerImpl.h"
#include "MyDate.h"

::grpc::Status CAdditionServerImpl::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignReply* response)
{
	std::string date = request->date();
	int clientMonth = ((date[0] - 48) * 10) + (date[1] - 48);
	int clientDay = ((date[3] - 48) * 10) + (date[4] - 48);
	MyDate clientDate(clientMonth, clientDay);
	for (int i = 0; i < v.size(); i++)
	{
		if (clientDate >= std::get<0>(v[i]) && clientDate <= std::get<1>(v[i]))
		{
			response->set_sign(std::get<2>(v[i]));
			return ::grpc::Status::OK;
		}
	}
}

