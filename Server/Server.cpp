
#include <iostream>
#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include"AdditionServerImpl.h"
#include <vector>
#include <string>
#include "MyDate.h"
#include <fstream>


int main()
{
    std::vector<std::tuple<MyDate, MyDate, std::string>> v;

    std::string line;
    std::ifstream myfile("Zodii.txt");
    while (std::getline(myfile, line))
    {
        int month1 = ((line[0] - 48) * 10) + (line[1] - 48);
        int day1 = ((line[3] - 48) * 10) + (line[4] - 48);
        MyDate date1(month1, day1);
        int month2 = ((line[6] - 48) * 10) + (line[7] - 48);
        int day2 = ((line[9] - 48) * 10) + (line[10] - 48);
        MyDate date2(month2, day2);
        std::string sign;
        for (int i = 12; i < line.length(); i++)
            sign.push_back(line[i]);
        v.push_back(std::make_tuple(date1, date2, sign));   
    }
    myfile.close();

    std::string server_address("localhost:8888");

    CAdditionServerImpl service(v);

    ::grpc_impl::ServerBuilder serverBuilder;
    serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    serverBuilder.RegisterService(&service);

    std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());

    std::cout << "Server listening on " << server_address << std::endl;

    server->Wait();
}
