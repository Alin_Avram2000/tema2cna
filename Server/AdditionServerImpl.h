#pragma once
#include "../Generated/SignOperation.grpc.pb.h"
#include "MyDate.h"

class CAdditionServerImpl final : public SignService::Service{
public:
	std::vector<std::tuple<MyDate, MyDate, std::string>> v;
	CAdditionServerImpl(std::vector<std::tuple<MyDate, MyDate, std::string>> v) {
		this->v = v;
	};
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignReply* response) override;
};