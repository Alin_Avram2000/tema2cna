#pragma once
class MyDate
{
public:
	int month;
	int day;
	MyDate(int month, int day);
	bool operator <= (const MyDate& d)
	{
		if (month == 12 && d.month == 1)
			return true;
		else
		{
			if (month < d.month)
				return true;
			else if (month == d.month)
			{
				if (day <= d.day)
					return true;
			}
		}
		return false;
	}

	bool operator >= (const MyDate& d)
	{
		if (month == 1 && d.month == 12)
			return true;
		else 
		{
			if (month > d.month)
				return true;
			else if (month == d.month)
			{
				if (day >= d.day)
					return true;
			}
		}
		return false;
	}
};

